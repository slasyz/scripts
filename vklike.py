#!/usr/bin/python3

import api.color
import api.output
import api.services.vk
import api.services.lastfm
from api.config import VK_USERID, LASTFM_USERNAME
from api.functions import eq, sigterm_handler, SigtermException

from signal import signal, SIGTERM
from argparse import ArgumentParser
from pprint import pprint

def get_vk_audio():
    """Get all audio from VK page"""
    result = []
    res = vk_req.request('audio.get', {'uid': VK_USERID})['response']

    for audio in res:
        result.append((audio['artist'], audio['title']))
    result_eq = [(eq(x[0]), eq(x[1])) for x in result]
    return result, result_eq

def get_loved_tracks():
    """Get all loved tracks on Last.FM"""
    result = []
    res = lastfm_req.request('user.getLovedTracks', 
                             {'user': LASTFM_USERNAME, 'limit': 999999}
                            )
    res = res['lfm'][0]['lovedtracks'][0]['track']
    
    for audio in res:
        result.append((str(audio['artist'][0]['name'][0]), 
                       str(audio['name'][0])
                     ))
    result.reverse()
    result_eq = [(eq(x[0]), eq(x[1])) for x in result]
    return result, result_eq

def main():
    vk_list, vk_list_eq = get_vk_audio()
    lastfm_list, lastfm_list_eq = get_loved_tracks()
    audio_list = [vk_list[i]
                  for i in range(len(vk_list_eq))
                  if lastfm_list_eq.count(vk_list_eq[i]) == 0]
    audio_list.reverse()
    api.output.debug('got {} tracks'.format(len(audio_list)))
    for i in range(len(audio_list)):
        artist, track = audio_list[i]
        res = lastfm_req.request('track.love',
                                 {'track': track.strip(), 'artist': artist.strip()}, 
                                 post = True
                                )
        api.output.info(api.color.number(i+1,
                                         len(audio_list),
                                         'track «{} - {}» liked'.format(artist, track),
                                         color = 'yellow'
                                        )
                       )

if __name__ == '__main__':
    try:
        signal(SIGTERM, sigterm_handler) # SIGTERM handler
        parser = ArgumentParser(description = 'Add tracks found on VK page to lastfm favorites.')
        parser.add_argument('-u', '--user',
                            type=str, dest="userid", default=VK_USERID,
                            help='Your VK user ID. Default value is "{}" and can be changed in api/config.py file.'.format(VK_USERID))
        parser.add_argument('-l', '--lastfm-user',
                            type=str, dest="username", default=LASTFM_USERNAME,
                            help='Your last.fm username. Default value is "{}" and can be changed in api/config.py file.'.format(LASTFM_USERNAME))
        args = parser.parse_args()
        VK_USERID = args.userid
        LASTFM_USERNAME = args.username

        api.output.debug_level = False
        vk_req = api.services.vk.VK(VK_USERID, '2994650', 'TKG0ZAtlwoOEwK0qPLU7')
        vk_req.auth('status,audio,offline')
        lastfm_req = api.services.lastfm.LastFM('e44f024998b4ccf7215bbc4242a2d00a', 'ac957f82517d2cfd299cefb05b4c456b')
        lastfm_req.auth()
        
        main()
    except KeyboardInterrupt:
        print()
        api.output.info('exit by keyboard interrupt')
    except SigtermException:
        api.output.info('exit by SIGTERM signal')
    #except (URLError, HTTPError):
    #    api.output.error('cannot set status')
    except (api.services.vk.VKError, api.services.lastfm.LastFMError) as err:
        print('{}: {}'.format(api.color.colorize(err.service, 'red'), err.msg))
        api.output.warning(err.request)
