Files
======

Main directory
--------------

Scripts:

* **2ch.py** - script for download pictures from thread on imageboard (or from VK album)
* **lasttags.py** - shows your personal tags on last.fm by play count increasing
* **simploader.py** - upload files to file hostings

VK:

* **vkdownload.py** - download audio files from VK page
* **vklike.py** - add tracks found on VK page to lastfm favorites
* **vkplay.py** - search music on VK and play it in *mplayer*
* **vkstatus.py** - set LastFM now playing to VK status

Other stuff:

* **gtk.sh** - shows GTK open file window and uploads selected files to Internet by *simploader.py*
* **screen.sh** - makes screenshot (with *scrot*) and uploads it to Internet
* **setup.py** - installs api/\* files to */usr/local/lib/python3.2/dist-packages* (*setup.py build && sudo setup.py install*)


./api/ directory
----------------

* **apibase.py** - main API classes
* **color.py** - module to work with colored shell output
* **config.py** contains your personal data (browser, VK ID, Last.fm username and other)
* **functions.py** - some stuff
* **multipart.py** - module to send multipart/form-data requests
* **output.py** - module to output log info
* **simploader.py** - simploader services

./api/services/ directory:

* **discogs.py** - Discogs API
* **lastfm.py** - Last.fm API
* **vk.py** - VK API
