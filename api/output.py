#!/usr/bin/python3

from time import strftime
from api.color import colorize

debug_level = False
quiet_level = False
filename = None
TIME_FORMAT = '%Y/%m/%d %H:%M:%S'

def progress(position, total, message, prefix='downloading'):
    """Prints progress of downloading/uploading/etc"""
    percent_int = round(100*position/total)
    position_kb = round(position/1024)
    total_kb = round(total/1024)
    
    if not quiet_level:
        if total == -1:
            percent_color = colorize('{} kbytes'.format(position_kb, 'purple'))
        else:
            percent_color = colorize('{}% ({} KiB/{} KiB)'.format(percent_int, position_kb, total_kb), 'green')
        result = '{}: {}'.format(colorize(message, 'lightblue'), percent_color)
        if prefix:
            result = '{} {}'.format(colorize(prefix, 'green'), result)
        print('\r\033[K{}'.format(result), end='')

def log(message, color, bold=True, linebreak=True):
    """Prints log information"""
    if len(message.split('\n')) > 1:
        message = '\n' + colorize(message, 'lightblue', bold = False)
    if not quiet_level:
        print('\r\033[K[{}] {}'.format(colorize(strftime(TIME_FORMAT), color, bold=bold), 
                                       message
                                      ),
             end = ''
            )
        if linebreak:
            print()
    if filename:
        f = open(filename, 'a')
        f.write('[{}] {}\n'.format(strftime(TIME_FORMAT), message))
        f.close()

def cinput(message, color = 'default'):
    """Reads text from stdin"""
    return input('\r\033[K[{}]: {}: '.format(colorize(strftime(TIME_FORMAT), color), 
                                             message
                                            )
                )

def info(message, linebreak=True):
    """Prints message with green appname"""
    log(message, 'green', linebreak=linebreak)
def error(message):
    """Prints message with red appname"""
    log(message, 'red')
def warning(message):
    """Prints message if level is debug with red appname"""
    if debug_level:
        log(message, 'red', bold=False)
def debug(message):
    """Prints message if level is debug with yellow appname"""
    if debug_level:
        log(message, 'yellow')
