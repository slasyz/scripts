#!/usr/bin/python3

import os
import re
import api.output

from http.client import HTTPResponse
from urllib.request import urlopen

class DownloadError(Exception):
    """Exception class for download errors"""
    pass

class SigtermException(Exception):
    """Exception lass for sigterm signal"""
    pass


def sigterm_handler(signum, frame):
    """Sigterm handler"""
    raise SigtermException

def pth(x):
    """Returns full absolute path"""
    return os.path.realpath(os.path.abspath(os.path.expanduser(x)))

def eq(title):
    """Removes all special characters and translates remaining in lower case"""
    pattern = r'[^\w\d]+'
    return re.sub(pattern, '', title.lower().strip())

def rm_if_exists(filename):
    try:
        os.remove(filename)
    except OSError:
        pass

def download(url, filename, chunk_size=8192, basename=None):
    """Download file from url to local file and output the process"""
    if isinstance(url, str):
        res = urlopen(url)
    elif isinstance(url, HTTPResponse):
        res = url
        url = res.url
    else:
        raise DownloadError
    
    destfile = open(filename, 'wb')
    has_length = res.getheader('Content-Length')
    total_size = -1
    if has_length:
        total_size = int(has_length.strip())
    pos = 0

    while 1:
        chunk = res.read(chunk_size)
        if not chunk: break
        if isinstance(chunk, str):
            chunk = bytes(chunk, res.info().get_content_charset())
        destfile.write(chunk)
        
        pos += len(chunk)
        api.output.progress(pos, total_size, basename or os.path.basename(url))

    res.close()
    destfile.close()
