import os.path, http.client, mimetypes

from api.output import progress

from io import IOBase, BytesIO
from urllib.request import urlopen, Request
from urllib.parse import urlsplit

BOUNDARY = '--------------------a2d3f8dcda2d3f8dcd7c9e2ea9470e93e34099ece8e1e7bb67c9e2ea9470eece8e1e7'+'bb6'

class LogIO(BytesIO):
    """BytesIO subclass for progress output"""
    def __init__(self, body, filename):
        self.__sizeof__ = len(body)
        self.filename = filename
        BytesIO.__init__(self, body)

    def read(self, size):
        progress(self.tell(), self.__sizeof__, self.filename, prefix = 'uploading')
        return BytesIO.read(self, size)

def get_mime(filename):
    """Get MIME-type"""
    return mimetypes.guess_type(filename)[0] or 'application/octet-stream'

def multipart_encode(fields):
    """Encode POST-params to multipart/form-data"""
    body = []
    for key, value in fields.items():
        body.append('--'+BOUNDARY)
        if isinstance(value, IOBase):
            body.append('Content-Disposition: form-data; name="{}"; filename="{}"'\
                        .format(key, os.path.basename(value.name))
                       )
            body.append('Content-Type: {}'.format(get_mime(value.name)))
            body.append('')
            body.append(value.read())
        else:
            body.append('Content-Disposition: form-data; name="{}"'.format(key))
            body.append('')
            body.append(value)
    body.append('--{}--'.format(BOUNDARY))
    body.append('')
    return b'\r\n'.join([bytes(x, 'utf-8') if isinstance(x, str) else x 
                         for x in body
                        ])

def request(url, fields, filename = ''):
    """Make multipart/form-data request"""
    scheme, host, path = urlsplit(url)[:3]
    if scheme == 'http': req = http.client.HTTPConnection(host)
    elif scheme == 'https': req = http.client.HTTPConnection(host)

    body = multipart_encode(fields)
    headers = {'Content-Type': 'multipart/form-data; boundary={}'.format(BOUNDARY),
               'Content-Length': str(len(body))
              }

    req.request('POST', url, LogIO(body, filename), headers)
    return req.getresponse()
