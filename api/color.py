COLORS = {'default':   '',
          'red':       ';31',
          'green':     ';32',
          'yellow':    ';33',
          'blue':      ';34',
          'purple':    ';35',
          'lightblue': ';36',
          'gray':      ';37',
         }

def colorize(text, color='default', bold=True):
    """Returns colored (by shell escape character) message to print it to stdout"""
    return '\033[{}{}m{}\033[0m'.format(int(bold), COLORS[color], text)

def number(num, total, message, color='default'):
    """Returns well-formatted numbered message"""
    return '\033[1{color}m[{0}/{1}]\033[0m {message}'\
           .format(str(num).rjust(len(str(total))),
                   total,
                   color = COLORS[color],
                   message = message
                  )