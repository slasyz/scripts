import os
import json
import atexit
import shutil

import api.apibase
from api.functions import pth, download
from api.multipart import request

from tempfile import mkdtemp
from urllib.request import urlopen, urlretrieve

imgur_apikey =      '8d8238c692079df0335e266b28d54c67' # API key for Imgur.com
imageshack_apikey = '17GKNSTUf19d48c56a70f778497c9e31c04385ed'

filehostings = ('imgur', 'imageshack', 'rghost')

class FileNotFound(Exception): pass
class WrongExtension(Exception): pass
class TooBig(Exception): pass
class UploadError(api.apibase.APIError): pass

class ImgurError(UploadError):
    """Exception class for Imgur errors"""
    service = 'Imgur'
class ImageshackError(UploadError):
    """Exception class for Imageshack errors"""
    service = 'ImageShack'
class RghostError(UploadError):
    """Exception class for Rghost errors"""
    service = 'Rghost'


class Service():
    """Basic class for all services"""
    def __init__(self, filepath):
        if filepath.startswith('http://'):
            tempdir = mkdtemp()[1]
            os.mkdir(tempdir)
            
            url = filepath
            filepath = self.filepath = os.path.join(tempdir,
                                                    os.path.basename(filepath)
                                                   )
            atexit.register(shutil.rmtree, tempdir, False)
            download(url, filepath)
        else:
            self.filepath = pth(filepath)

        if not os.path.exists(filepath):
            raise FileNotFound
        if self.extensions and (os.path.splitext(filepath)[1][1:] not in self.extensions): # check extension
            raise WrongExtension
        if os.path.getsize(filepath) > self.max_file_size: # check file size
            raise TooBig


class Imgur(Service):
    """Imgur class. http://imgur.com/"""
    max_file_size = 5242880
    extensions = ('jpg', 'jpeg', 'png', 'bmp', 'gif')

    def upload(self):
        request_url = 'http://api.imgur.com/2/upload.json'
        params = {'key': imgur_apikey, 'image': open(self.filepath, 'rb'), 'type': 'file'}
        res_txt = request(request_url,
                          params,
                          os.path.basename(self.filepath)
                         ).read()
        try:
            res = json.loads(str(res_txt, 'utf-8'))
            self.link = res['upload']['links']['original']
        except KeyError:
            raise ImgurError(res['error']['message'], params)
        except ValueError:
            raise ImgurError


class Rghost(Service):
    """Rghost class. http://rghost.ru/"""
    max_file_size = 52428800
    extensions = ()
    
    def upload(self):
        request_url = 'http://pion.rghost.ru/files'
        res = request(request_url, 
                      {'file': open(self.filepath, 'rb')}, 
                      os.path.basename(self.filepath)
                     ).getheader('Location', None)
        if not res: raise RghostError
        self.link = res.replace('pion.rghost.ru', 'rghost.ru')


class Imageshack(Service):
    """Imageshack class. http://imageshack.us/"""
    max_file_size = 13145728
    extensions = ('jpg', 'jpeg', 'png', 'bmp', 'gif')
    
    def upload(self):
        request_url = 'http://www.imageshack.us/upload_api.php'
        params = {'fileupload': open(self.filepath, 'rb'), 
                  'key': imageshack_apikey
                 }
        res = api.apibase.XMLResponse(request(request_url, 
                                              params, 
                                              os.path.basename(self.filepath)
                                             ).read()
                                     )

        try:
            self.link = str(res['imginfo'][0]['links'][0]['image_link'][0])
        except (KeyError, IndexError):
            raise ImageshackError(str(res['links'][0]['error'][0]), params)


class Test(Service):
    """Test class. Nevermind"""
    max_file_size = 2**64
    extensions = ()
    
    def upload(self):
        request_url = 'http://localhost/asdf.php'
        params = {'filefield': open(self.filepath, 'rb'), 'asdf': 'fddddd'}
        res = request(request_url,
                      params,
                      os.path.basename(self.filepath)
                     ).read()

        print('\033[K'+str(res, 'utf-8'))
        exit()
