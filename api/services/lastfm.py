#!/usr/bin/python3
# coding: utf-8

import os.path
import socket

import api.apibase
import api.output
from api.config import BROWSER
from api.functions import pth

from hashlib import md5
from urllib.error import URLError, HTTPError
from http.client import BadStatusLine

class LastFMError(api.apibase.APIError):
    """Exception class for Last.FM API errors"""
    service = 'LastFM'

class LastFM(api.apibase.API):
    """Class for work with Last.FM API"""
    name = 'LastFM'
    filename = '~/.api.lastfm'

    def __init__(self, api_key, api_secret = ''):
        self.API_KEY = api_key
        self.API_SECRET = api_secret
        self.FILE = pth(self.filename)
        self.get_session()

    def make_signature(self, method, params):
        """Make API signature"""
        keyz = list(params.keys())
        keyz.sort()

        token_str = ''.join(['{}{}'.format(key,params[key]) for key in keyz])
        token = md5((token_str+self.API_SECRET).encode('utf-8')).hexdigest()
        return token
    
    def auth(self):
        """Last.FM authentication"""
        api.output.debug('LastFM: beginning authentication')
        if self.SESSION_KEY == '':
            # 1. get token
            token = self.request('auth.getToken')['lfm'][0]['token'][0].text()

            # 2. request authorization
            from subprocess import call
            devnull = open(os.path.devnull, 'w')

            url = 'http://www.last.fm/api/auth/?api_key={}&token={}'\
                  .format(self.API_KEY, token)
            call([BROWSER, url], stdout=devnull, stderr=devnull)
            input_str = api.output.cinput('LastFM: give me a permission to access your data and then press <enter>', 'green')

            # 3. fetch a session
            res = self.request('auth.getSession', {'token': token})
            self.SESSION_KEY = res['lfm'][0]['session'][0]['key'][0].text()

            # saving session key to file
            self.save_session()

            api.output.info('LastFM: authentication is successful')

    def request(self, method, params = {}, post=False):
        """Make Last.FM API request. More info on http://last.fm/api"""
        from urllib.parse import urlencode
        from urllib.request import urlopen

        api.output.debug('LastFM: making "{}" request with "{}" params'.format(method, params))
        if method[:4] != 'auth':
            params['sk'] = self.SESSION_KEY
        params['method'] = method
        params['api_key'] = self.API_KEY
        params['api_sig'] = self.make_signature(method, params)

        url = 'http://ws.audioscrobbler.com/2.0/?{}'
        url = url.format(urlencode(params))
        result = None
        while not result:
            try:
                if post:
                    result = urlopen('http://ws.audioscrobbler.com/2.0/', 
                                     urlencode(params).encode('utf-8')
                                    ).read()
                else:
                    result = urlopen(url).read()
            except HTTPError as err:
                if (err.getcode() / 100) == 4: # request error
                    result = err.read()
                else:
                    api.output.warning('LastFM: HTTP Error. Trying again')
            except (URLError, BadStatusLine, socket.error):
                api.output.warning('LastFM: HTTP Error. Trying again')

        api.output.debug(str(result, 'utf-8'))
        res = api.apibase.XMLResponse(result)
        if res['lfm'][0].status == 'failed':
            raise LastFMError(str(res['lfm'][0]['error'][0]), int(res['lfm'][0]['error'][0].code), params)
        return res
