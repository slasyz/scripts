#!/usr/bin/python3
# coding: utf-8

import os.path
import json

import api.apibase
from api.config import BROWSER
from api.functions import pth

from hashlib import md5
from pprint import pprint

from urllib.error import URLError, HTTPError
from http.client import BadStatusLine

class VKError(api.apibase.APIError):
    """Exception class for VK API errors"""
    service = 'VK'

class VK(api.apibase.API):
    """Class for work with VK API"""
    name = 'VK'
    filename = '~/.api.vk'

    def __init__(self, user_id, api_key, api_secret = ''):
        self.API_KEY = api_key
        self.API_SECRET = api_secret
        self.USER_ID = user_id
        self.FILE = pth(self.filename)
        self.get_session()

    def make_signature(self, method, params):
        """Make API signature"""
        keyz = list(params.keys())
        keyz.sort()

        token_str = ''.join(['{}={}'.format(key, params[key]) for key in keyz])
        token = md5((self.USER_ID+token_str+self.API_SECRET).encode('utf-8')).hexdigest()
        return token

    def auth(self, scope):
        """VK authentication"""
        api.output.debug('VK: beginning authentication')
        if self.SESSION_KEY == '':
            # getting new session
            from subprocess import call
            devnull = open(os.path.devnull, 'w')

            # request authorization
            url = 'http://oauth.vk.com/authorize?client_id={}&scope={}&redirect_uri=http://api.vk.com/blank.html&response_type=token'.format(self.API_KEY, scope)
            call([BROWSER, url], stdout=devnull, stderr=devnull)
            self.SESSION_KEY = api.output.cinput('VK: give permission to access your status and then enter here access token', 'green')

            # saving session key to file, if 'scope' has 'offline' option
            if 'offline' in scope.split(','):
                self.save_session()

            api.output.info('VK: authentication is successful')

    def request(self, method, params = {}):
        """Make Last.FM API request. More info on http://vk.com/pages?oid=-1&p=%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F"""
        from urllib.parse import urlencode
        from urllib.request import urlopen

        api.output.debug('VK: making "{}" request with "{}" params'.format(method, params))
        params['method'] = method
        params['api_id'] = self.API_KEY
        params['format'] = 'JSON'
        params['sig'] = self.make_signature(method, params)

        url = 'https://api.vk.com/method/{}?{}&access_token={}'
        url = url.format(method, urlencode(params), self.SESSION_KEY)

        result = None
        while not result:
            try:
                result = str(urlopen(url).read(), 'utf-8')
            except (URLError, HTTPError, BadStatusLine):
                api.output.warning('VK: HTTP Error. Trying again')
        
        api.output.debug(result)
        res = api.apibase.json_unescape(json.loads(result))
        if 'error' in res.keys():
            raise VKError(res['error']['error_msg'], res['error']['error_code'], params)
        return res
