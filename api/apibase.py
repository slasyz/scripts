#!/usr/bin/python3

import sys
import os.path

import api.output
from html.parser import HTMLParser
from urllib.parse import urlencode
from xml.dom.minidom import parseString, Element

def json_unescape(dictionary):
    """Unescapes all json elements"""
    if isinstance(dictionary, dict):
        for key, value in dictionary.items():
            dictionary[key] = json_unescape(value)
    elif isinstance(dictionary, list):
        for i in range(len(dictionary)):
            dictionary[i] = json_unescape(dictionary[i])
    elif isinstance(dictionary, str):
        dictionary = HTMLParser().unescape(dictionary)
    return dictionary

class APIError(Exception):
    """Exception class for API errors"""
    service = 'API'
    def __init__(self, msg='no description', no=None, request=None):
        self.msg = msg
        if isinstance(request, dict):
            request = urlencode(request)
        self.request = request
        if no:
            self.no = no
    def __str__(self):
        return self.msg

class XMLResponse():
    def __init__(self, doc):
        if isinstance(doc, (str, bytes)):
            self.doc = parseString(doc)
        elif isinstance(doc, Element):
            self.doc = doc
        else:
            raise APIError('Error with response')

    def get_tags(self, tag):
        """Returns list of tags located in the root of document"""
        needed_tags = []
        childnodes = [x 
                      for x in self.doc.childNodes 
                      if x.nodeType == x.ELEMENT_NODE
                     ]
        for child in childnodes:
            if child.tagName == tag:
                needed_tags.append(XMLResponse(child))
        return needed_tags
    __getitem__ = get_tags # for obj[tag] syntax
            
    def get_attr(self, attr):
        """Returns value of attribute"""
        if self.doc.hasAttribute(attr):
            return HTMLParser().unescape(self.doc.getAttribute(attr))
        else:
            return None
    __getattr__ = get_attr # for obj.attr

    def text(self):
        """Returns text of tag (implying that tag have no tags inside)"""
        childnodes = self.doc.childNodes
        if len(childnodes) == 0:
            return ''
        else:
            return HTMLParser().unescape(str(childnodes[0].data.strip()))
    __str__ = text

class API():
    SESSION_KEY = None
    
    def get_session(self):
        """Get session key from file"""
        if os.path.exists(self.FILE):
            api.output.debug('getting session key from file')
            f = open(self.FILE)
            file_content = f.readlines()
            f.close()

            for key in file_content:
                if key.split('|')[0] == self.API_KEY:
                    self.SESSION_KEY = key.split('|')[1]
                    return

        self.SESSION_KEY = ''
        api.output.debug('session key file doesn\'t exist. need to make auth() function')

    def save_session(self):
        """Save session key to file"""
        if os.path.exists(self.FILE):
            f = open(self.FILE)
            file_content = f.read().strip().split('\n')
            f.close()
        else:
            file_content = []
        
        flag = True
        string = '{}|{}'.format(self.API_KEY, self.SESSION_KEY)
        for key in file_content:
            if key.split('|')[0] == self.API_KEY:
                file_content[file_content.index()] = string
                flag = False
        if flag:
            file_content.append(string)
        
        f = open(self.FILE, 'w')
        f.write('\n'.join(file_content))
        f.close()
        api.output.debug('writing session key to file')
