#!/bin/bash

FILE=`date +"%Y-%m-%d_%H-%M-%S".png`
SERVICE=${1:-imgur}
DIR=`dirname \`readlink -f $0\``

import -quality 100 /tmp/$FILE
$DIR/simploader.py -s $SERVICE /tmp/$FILE
rm -f "/tmp/$FILE"
