#!/usr/bin/python3
# coding: utf-8

import os, sys, re
import atexit

import api.color
import api.output
import api.services.vk
from api.config import VK_USERID, VK_MUSIC_DIR
from api.functions import eq, pth, rm_if_exists, download
from api.functions import sigterm_handler, SigtermException

from urllib.request import urlretrieve
from argparse import ArgumentParser
from tempfile import mkstemp

from subprocess import call
from signal import signal, SIGTERM
from shutil import move, rmtree

from urllib.error import URLError, HTTPError
from http.client import BadStatusLine

FILENAME = '{artist}/{artist} - {track}.mp3'
ID3SCRIPT = 'id3v2 --artist "{artist}" --song "{track}" "{filename}"'
DIRECTORY = VK_MUSIC_DIR

def remove_symbols(filename):
    """Remove all symbols from file name"""
    pattern = r'[\\\/\:\*\?\"\<\>\|]+'
    return re.sub(pattern, '-', filename).replace('_', ' ').strip()

def vkdownload(data, filename):
    """Download audio file from VK"""
    try:
        url = data['url']
        
        tmp_filename = mkstemp()[1]
        atexit.register(rm_if_exists, tmp_filename)
        api.output.debug('beginning urlretrieve {}'.format(url))
        download(url, tmp_filename, basename = os.path.basename(filename))

        api.output.debug('setting id3 tag')
        devnull = open(os.path.devnull, 'w')
        call(ID3SCRIPT.format(artist = data['artist'],
                              track = data['title'],
                              filename = tmp_filename
                             ),
             stdout = devnull, stderr = devnull, shell = True
            )
        devnull.close()

        try: os.makedirs(os.path.dirname(filename))
        except: pass
        move(tmp_filename, filename)
        return True
    except (URLError, HTTPError, BadStatusLine):
        return False

def get_audio_list():
    """Get all audio from VK page"""
    result = []
    xml = req.request('audio.get', {'uid': VK_USERID})['response']

    for audio in xml:
        artist = remove_symbols(audio['artist'])
        track = remove_symbols(audio['title'])
        result.append(audio)
    return result

def search_for_duplicates(audio_list):
    """Search for duplicates on VK page"""
    audio_list_eq = [eq('{}{}'.format(x['artist'], x['title'])) 
                     for x in audio_list
                    ]
    i = 0
    remove_all_duplicates = skip_all = False
    while i < len(audio_list_eq):
        track_name = '{0[artist]} - {0[title]}'.format(audio_list[i])
        cnt = audio_list_eq.count(audio_list_eq[i])
        if cnt > 1:
            inp = 'asdf'
            agree = ('', 'y', 'a', 'yes', 'all')
            
            if remove_all_duplicates:
                api.output.error('found a duplicate: «{}»; removing it'.format(track_name), 'red')
            elif not skip_all:
                inp = api.output.cinput('found a duplicate: {} x «{}»; remove it? {}, [n]o, [a]ll, [s]kip all'\
                                       .format(cnt, track_name, api.color.colorize('[y]es', 'red')), 'red')
                if inp in ('a', 'all'): remove_all_duplicates = True
                elif inp in ('s', 'skip', 'skip all'): skip_all = True
            
            trackname_eq = audio_list_eq[i]
            while audio_list_eq.count(trackname_eq) > 1:
                j = audio_list_eq.index(trackname_eq)
                if remove_all_duplicates or (inp in agree):
                    oid = audio_list[j]['owner_id'];
                    aid = audio_list[j]['aid']
                    
                    api.output.debug('removing audio {}_{}'.format(oid, aid))
                    res = req.request('audio.delete', {'oid': oid, 'aid': aid})
                    api.output.debug('audio removed')
                del audio_list[j], audio_list_eq[j]
        else:
            i+=1

    return audio_list

def main():
    if (not os.path.exists(stat_file)) or args.rld:
        try: os.makedirs(DIRECTORY)
        except: pass
        f = open(stat_file, 'w')
        audio_list_downloaded = []

        # cleaning DIRECTORY
        dir_content = os.listdir(DIRECTORY)
        for fl in dir_content:
            if fl != '.filelist':
                fl_full = os.path.join(DIRECTORY, fl)
                if os.path.isdir(fl_full):
                    rmtree(fl_full)
                else:
                    os.remove(fl_full)
    else:
        f = open(stat_file, 'r+')
        audio_list_downloaded = f.read().strip().split('\n')

    api.output.info('getting audio list')
    audio_list = get_audio_list()
    audio_list.reverse()

    api.output.info('search for duplicates')
    audio_list = search_for_duplicates(audio_list)

    api.output.info('beginning downloading')
    for i in range(len(audio_list)):
        data = audio_list[i]
        filename = FILENAME.format(artist = data['artist'], track = data['title'])
        message = api.color.number(i+1, len(audio_list), filename, 'yellow')
        if filename not in audio_list_downloaded:
            if vkdownload(data, filename):
                api.output.info(message)
                f.write('{}\n'.format(filename))
            else:
                api.output.error(message)
        else:
            api.output.info(message)
    f.close()

if __name__ == '__main__':
    try:
        parser = ArgumentParser(description = 'Download audio files from VK page.')
        parser.add_argument('-f', '--reload',
                            action="store_true", dest="rld", default=False,
                            help="Reload all files")
        parser.add_argument('-v', '--verbose',
                            action="store_true", dest="verbose", default=False,
                            help="Verbose output")
        parser.add_argument('-d', '--directory',
                            type=str, dest="directory", default=DIRECTORY,
                            help='Directory which will contain downloaded files (should be empty or contain downloaded files by this script). Default value is "{}" and can be changed in api/config.py file.'.format(DIRECTORY))
        parser.add_argument('-u', '--user',
                            type=str, dest="userid", default=VK_USERID,
                            help='Your user ID. Default value is "{}" and can be changed in api/config.py file.'.format(VK_USERID))
        parser.add_argument('-t', '--filenames',
                            type=str, dest="filename", default=FILENAME,
                            help='File names template. You can use "{{artist}}" and "{{track}}" variables. Default value is "{}" and can be changed in api/config.py file.'.format(FILENAME))

        args = parser.parse_args()
        VK_USERID = args.userid
        DIRECTORY = pth(args.directory)
        FILENAME = os.path.join(*args.filename.split('/'))
        stat_file = os.path.join(DIRECTORY, '.filelist')

        req = api.services.vk.VK(VK_USERID, '2994650', 'TKG0ZAtlwoOEwK0qPLU7')
        req.auth('status,audio,offline')
        api.output.debug_level = args.verbose
        signal(SIGTERM, sigterm_handler) # SIGTERM handler

        os.chdir(DIRECTORY)
        main()
    except KeyboardInterrupt:
        api.output.info('exit by keyboard interrupt')
    except SigtermException:
        api.output.info('exit by SIGTERM signal')
    except api.services.vk.VKError as err:
        api.output.error(err.msg)
