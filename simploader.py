#!/usr/bin/python3

import os, sys

import api.color
import api.output
import api.simploader
from api.functions import pth
from api.functions import sigterm_handler, SigtermException

from pprint import pprint
from subprocess import call
from argparse import ArgumentParser
from time import strftime

from signal import signal, SIGTERM
from urllib.error import URLError, HTTPError
from http.client import BadStatusLine

LOG_FILE = '~/.simploader.log'

class WrongServiceName(Exception):
    """Exception class for wrong service name"""
    pass

if LOG_FILE and not os.path.exists(pth(LOG_FILE)):
    open(pth(LOG_FILE), 'w').close()

def main():
    for filename in args.files:
        basename = api.color.colorize(os.path.basename(filename), color = 'yellow')
        try:
            req = api.simploader.__dict__[args.service.capitalize()](filename)
            req.upload()

            api.output.info('{}: {}'.format(basename, req.link))
            os.popen('xclip', 'w').write(req.link)
            call(['notify-send', os.path.basename(filename), req.link], 
                 stdout = devnull,
                 stderr = devnull
                )
            if LOG_FILE:
                f = open(pth(LOG_FILE), 'a')
                f.write('{}: [{}] {}\n'.format(api.output.colorize(strftime(api.output.TIME_FORMAT), 'green', bold=True),
                                               basename,
                                               req.link
                                              )
                       )
                f.close()
        except api.simploader.FileNotFound:
            api.output.error('{}: file not found'.format(basename))
        except api.simploader.WrongExtension:
            api.output.error('{}: wrong file type for that file hosting'.format(basename))
        except api.simploader.TooBig:
            api.output.error('{}: file is too big for that file hosting. Try another'.format(basename))
        except api.simploader.UploadError as err:
            api.output.error('{}: {}'.format(basename, err.msg))

if __name__ == '__main__':
    try:
        parser = ArgumentParser(description = 'Upload files to file hostings')
        parser.add_argument('files',
                            type=str, nargs='+',
                            help="List of files")
        parser.add_argument('-s', '--service',
                            type=str, dest="service", default='imgur',
                            help='Service name. Available services: {}'\
                                 .format(', '.join(api.simploader.filehostings))
                           )
        args = parser.parse_args()

        if args.service not in api.simploader.filehostings + ('test',):
            raise WrongServiceName

        api.output.debug_level = True
        signal(SIGTERM, sigterm_handler) # SIGTERM handler
        devnull = open('/dev/null', 'w')

        main()
    except WrongServiceName:
        api.output.error('wrong service name. Available services: {}'\
                  .format(', '.join(services.filehostings))
                 )
    except KeyboardInterrupt:
        api.output.info('exit by keyboard interrupt')
    except SigtermException:
        api.output.info('exit by sigterm signal')
