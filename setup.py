#!/usr/bin/python3

from distutils.core import setup

setup(name='apisl',
      version='0.01',
      description='Some basic functions for my scripts',
      author='Vyacheslav S.',
      author_email='slasyz@gmail.com',
      url='http://slasyz.ru/',
      packages=['api', 'api.services'],
     )
