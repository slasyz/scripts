#!/usr/bin/python3
# coding: utf-8

import os
import re
import atexit

import api.color
import api.output
import api.services.vk
import api.services.lastfm
from api.config import VK_USERID, LASTFM_USERNAME
from api.functions import eq, sigterm_handler, SigtermException

from signal import signal, SIGTERM
from argparse import ArgumentParser
from time import sleep

from urllib.error import URLError, HTTPError
from http.client import BadStatusLine

def get_audio_id(query):
    """Find audio on VK"""
    result = None
    if eq(query):
        #query = re.sub(r'[^\d\w]+', ' ', query)
        api.output.debug('search for «{}»'.format(query))
        songs = vk_req.request('audio.search',
                               {'q': query, 'count': '40'}
                              )
        songs = songs['response'][1:]
        for song in songs:
            aid = '{0[owner_id]}_{0[aid]}'.format(song)
            if '{} - {}'.format(song['artist'],song['title']) == query:
                api.output.debug('found perfect match: {}'.format(aid))
                return aid
            if eq(song['artist']+song['title']) == eq(query):
                api.output.debug('found: {}'.format(aid))
                result = aid
    if result:
        api.output.debug('found something like: {}'.format(result))
        return result
    else:
        api.output.debug('audio wasn\'t found, setting plain text status')
        return result

def set_status(text = ''):
    """Set status"""
    colored_text = api.color.colorize(text, 'lightblue')
    try:
        audio_id = get_audio_id(text)
        if not audio_id: raise api.services.vk.VKError(no=221)
        res = vk_req.request('status.set', {'audio': audio_id})['response']
        api.output.info('audio status has been set: «{}» (id: {})'\
                        .format(colored_text, audio_id)
                       )
    except api.services.vk.VKError as err:
        if err.no != 221:
            raise err
        res = vk_req.request('status.set', {'text': ''})
        api.output.info('status has been set to empty')
    return text

def main():
    last_song = vk_req.request('status.get')['response']['text']
    flag = False
    new_status = ''
    while True:
        try:
            track = lastfm_req.request('user.getRecentTracks',
                                       {'user': LASTFM_USERNAME, 'limit': '1'}
                                      )
            track = track['lfm'][0]['recenttracks'][0]['track'][0]
            artist = track['artist'][0]
            name = track['name'][0]
            if track.nowplaying:
                new_status = '{artist} - {song}'.format(artist = artist, song = name)

            if new_status != last_song:
                last_song = set_status(new_status)
        except (URLError, HTTPError, BadStatusLine, IndexError):
            if api.output.debug_level:
                api.output.error('HTTP Error')

        sleep(10)

if __name__ == '__main__':
    try:
        signal(SIGTERM, sigterm_handler) # SIGTERM handler
        parser = ArgumentParser(description = 'Set LastFM now playing to VK status.')
        parser.add_argument('-v', '--verbose',
                            action="store_true", dest="verbose", default=False,
                            help="Verbose output")
        parser.add_argument('-q', '--quiet',
                            action="store_true", dest="quiet", default=False,
                            help="No output")
        parser.add_argument('-u', '--user',
                            type=str, dest="userid", default=VK_USERID,
                            help='Your VK user ID. Default value is "{}" and can be changed in api/config.py file.'.format(VK_USERID))
        parser.add_argument('-l', '--lastfm-user',
                            type=str, dest="username", default=LASTFM_USERNAME,
                            help='Your last.fm username. Default value is "{}" and can be changed in api/config.py file.'.format(LASTFM_USERNAME))
        parser.add_argument('-p', '--pidfile',
                            type=str, dest="pidfile", default=None,
                            help='Process PID file.')
        args = parser.parse_args()
        VK_USERID = args.userid
        LASTFM_USERNAME = args.username

        api.output.debug_level = args.verbose
        api.output.quiet_level = args.quiet
        api.output.filename = os.path.expanduser('~/.vkstatus.log')
        if args.pidfile:
            try:
                f = open(args.pidfile, 'w')
            except IOError as err:
                if err.errno == 13:
                    api.output.error('failed to create pid file')
                else:
                    raise err
            else:
                f.write(str(os.getpid()))
                f.close()
        vk_req = api.services.vk.VK(VK_USERID, '2994650', 'TKG0ZAtlwoOEwK0qPLU7')
        vk_req.auth('status,audio,offline')
        lastfm_req = api.services.lastfm.LastFM('e44f024998b4ccf7215bbc4242a2d00a')
        atexit.register(set_status)

        main()
    except KeyboardInterrupt:
        api.output.info('exit by keyboard interrupt')
    except SigtermException:
        api.output.info('exit by SIGTERM signal')
    except (URLError, HTTPError):
        api.output.error('cannot set status')
    except (api.services.vk.VKError, api.services.lastfm.LastFMError) as err:
        if not api.output.quiet_level:
            print('{}: {}'.format(api.color.colorize(err.service, 'red'), err.msg))
            api.output.warning(err.request)
